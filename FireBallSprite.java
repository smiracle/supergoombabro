
// FireBallSprite.java

/* A fireball starts at the lower right hand side of the panel,
   and travels straight across to the left (at varying speeds).
   If it hits 'goomba', it explodes (with a suitable explosion sound).

   A fireball that has left the left hand side, or exploded, is
   reused.
*/

import java.awt.*;


public class FireBallSprite extends Sprite
{
  // the ball's x- and y- step values are STEP +/- STEP_OFFSET
  private static final int STEP = -10;   // moving left
  private static final int STEP_OFFSET = 2;

  private GoombaPanel gp;    // tell GoombaPanel about colliding with goomba
  private GoombaSprite goomba;


  public FireBallSprite(int w, int h, ImagesLoader imsLd,
                               GoombaPanel gp, GoombaSprite g) 
  { super( w, h/2, w, h, imsLd, "fireball");  
        // the ball is positioned in the middle at the panel's rhs
    this.gp = gp;
    goomba = g;
    initPosition();
  } // end of FireBallSprite()


  private void initPosition()
  // adjust the fireball's position and its movement left
  {
    int h = getPHeight()/2 + ((int)(getPHeight() * Math.random())/2);
                     // along the lower half of the rhs edge
    if (h + getHeight() > getPHeight())
      h -= getHeight();    // so all on screen

    setPosition(getPWidth(), h);   
    setStep(STEP + getRandRange(STEP_OFFSET), 0);   // move left
  } // end of initPosition()


  private int getRandRange(int x) 
  // random number generator between -x and x
  {   return ((int)(2 * x * Math.random())) - x;  }



  public void updateSprite() 
  { hasHitGoomba();
    goneOffScreen();
    super.updateSprite();
  }


  private void hasHitGoomba()
  /* If the ball has hit Goomba, tell GoombaPanel (which will
     display an explosion and play a clip), and begin again.
  */
  { 
    Rectangle goombaBox = goomba.getMyRectangle();
    goombaBox.grow(-goombaBox.width/3, 0);   // make goomba's bounded box thinner

    if (goombaBox.intersects( getMyRectangle() )) {    // goomba collision?
      gp.showExplosion(locx, locy+getHeight()/2);  
             // tell GoombaPanel, supplying it with a hit coordinate
      initPosition();
    }
  } // end of hasHitGoomba()


  private void goneOffScreen()
  // when the ball has gone off the lhs, start it again.
  {
    if (((locx+getWidth()) <= 0) && (dx < 0)) // off left and moving left
      initPosition();   // start the ball in a new position
  }  // end of goneOffScreen()


}  // end of FireBallSprite class
