
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;


public class SuperGoombaBro extends JFrame implements WindowListener
{
  private static int DEFAULT_FPS = 30;      // 40 is too fast! 

  private GoombaPanel gp;        // where the game is drawn
  private MidisLoader midisLoader;
  

  public SuperGoombaBro(long period)
  { super("SuperGoombaBro");

    // load the background MIDI sequence
    //midisLoader = new MidisLoader();
    //midisLoader.load("jjf", "mymidiNAMEHERE.mid");
    //midisLoader.play("jjf", true);   // repeatedly play it

    Container c = getContentPane();    // default BorderLayout used
    gp = new GoombaPanel(this, period);
    c.add(gp, "Center");

    addWindowListener( this );
    pack();
    setResizable(false);
    setVisible(true);
  }  // end of class constructor


  // ----------------- window listener methods -------------

  public void windowActivated(WindowEvent e) 
  { gp.resumeGame();  }

  public void windowDeactivated(WindowEvent e) 
  { gp.pauseGame();  }


  public void windowDeiconified(WindowEvent e) 
  {  gp.resumeGame();  }

  public void windowIconified(WindowEvent e) 

  {  gp.pauseGame(); }

  public void windowClosing(WindowEvent e)
  {  gp.stopGame();  
    // midisLoader.close();  // not really required
  }


  public void windowClosed(WindowEvent e) {}
  public void windowOpened(WindowEvent e) {}

  // ----------------------------------------------------

  public static void main(String args[])
  { 
    long period = (long) 1000.0/DEFAULT_FPS;
    // System.out.println("fps: " + DEFAULT_FPS + "; period: " + period + " ms");
    new SuperGoombaBro(period*1000000L);    // ms --> nanosecs 
  }

} // end of class


